import express, { Request, Response } from "express";
import productosService from "./productos.service";

const router = express.Router();

router.use(express.json());

router.get("/id/:id", async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let producto = await productosService.getProductosByID(id);
        if (!producto)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.json(producto);
    } catch (error) {
        res.status(404).send(`No se encontró el producto con id: ${id}`);
    }
});

export default router;